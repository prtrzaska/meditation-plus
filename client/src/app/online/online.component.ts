import { Subscription } from 'rxjs';
import { Component, Input, Injectable, OnChanges, OnDestroy } from '@angular/core';
import { UserService } from '../user';

@Injectable()
@Component({
  selector: 'online',
  templateUrl: './online.component.html',
  styleUrls: [
    './online.component.styl'
  ]
})
export class OnlineComponent implements OnChanges, OnDestroy {

  @Input() detailed = true;

  socketSubscription: Subscription;
  onlineCounter = 0;
  onlineUsers: Array<any> = [];

  constructor(private userService: UserService) {
    // initially load online counter
    this.userService.getOnlineCount()
      .subscribe(res => this.onlineCounter = res);

    // intialize socket
    this.socketSubscription = userService.getOnlineSocket().subscribe(res => {
      this.onlineCounter = res;
      if (this.onlineCounter > 0 && this.detailed) {
        this.loadOnlineUsers();
      }
    });

    if (this.detailed) {
      this.loadOnlineUsers();
    }
  }

  loadOnlineUsers() {
    this.userService.getOnlineUsers()
      .subscribe(res => this.onlineUsers = res);
  }

  ngOnChanges() {
    if (this.detailed) {
      this.loadOnlineUsers();
    }
  }

  ngOnDestroy() {
    this.socketSubscription.unsubscribe();
  }
}
