import { Component } from '@angular/core';
import { CommitmentService } from '../../commitment';
import { DialogService } from '../../dialog/dialog.service';
import { filter, concatMap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { AppState } from '../../reducers';
import { MatSnackBar } from '@angular/material';
import { ThrowError } from '../../actions/global.actions';

@Component({
  selector: 'commitment-admin',
  templateUrl: './commitment-admin.component.html',
  styleUrls: [
    './commitment-admin.component.styl'
  ]
})
export class CommitmentAdminComponent {

  // commitment data
  commitments: Object[] = [];

  constructor(
    private commitmentService: CommitmentService,
    private dialog: DialogService,
    private store: Store<AppState>,
    private snackbar: MatSnackBar
  ) {
    this.loadCommitments();
  }

  /**
   * Loads all commitments
   */
  loadCommitments() {
    this.commitmentService
      .getAll()
      .subscribe(res => this.commitments = res);
  }

  delete(evt, commitment) {
    evt.preventDefault();

    this.dialog.confirmDelete().pipe(
      filter(val => !!val),
      concatMap(() => this.commitmentService.delete(commitment))
    ).subscribe(
      () => {
        this.snackbar.open('Commitment has been deleted successfully.');
        this.loadCommitments();
      },
      error => this.store.dispatch(new ThrowError({ error }))
    );
  }
}
