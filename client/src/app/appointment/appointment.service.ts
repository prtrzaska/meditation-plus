import { Injectable } from '@angular/core';
import { ApiConfig } from '../../api.config';
import { WebsocketService } from '../shared';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { concatMap } from 'rxjs/operators';

@Injectable()
export class AppointmentService {

  public constructor(
    private http: HttpClient,
    private wsService: WebsocketService
  ) {
  }

  public getAll(): Observable<any> {
    return this.http.get(ApiConfig.url + '/api/appointment');
  }

  public get(id: string): Observable<any> {
    return this.http.get(ApiConfig.url + '/api/appointment/' + id);
  }

  public getAggregated(): Observable<any> {
    return this.http.get(ApiConfig.url + '/api/appointment/aggregated');
  }

  public save(appointment): Observable<any> {
    const method = appointment._id ? 'put' : 'post';

    return this.http[method](
      ApiConfig.url + '/api/appointment' + (appointment._id ? '/' + appointment._id : ''),
      appointment,
      { observe: 'response', responseType: 'text' }
    );
  }

  public registration(appointment): Observable<any> {
    return this.http.post(
      `${ApiConfig.url}/api/appointment/${appointment._id}/register`,
      ''
    );
  }

  /**
   * Initializes Socket.io client with Jwt and listens to 'appointment'.
   */
  public getSocket(): Observable<any> {
    return this.wsService.getSocket().pipe(
      concatMap(websocket => Observable.create(obs => {
        websocket.on('appointment', res => obs.next(res));
      }))
    );
  }

  public delete(appointment): Observable<any> {
    return this.http.delete(ApiConfig.url + '/api/appointment/' + appointment._id);
  }

  public deleteRegistration(appointment): Observable<any> {
    return this.http.delete(ApiConfig.url + '/api/appointment/remove/' + appointment._id);
  }

  public toggle(hour: number, day: number): Observable<any> {
    return this.http.post(
      ApiConfig.url + '/api/appointment/toggle',
      { hour, day },
      { observe: 'response', responseType: 'text' }
    );
  }

  public update(oldHour: number, newHour: number): Observable<any> {
    return this.http.post(ApiConfig.url + '/api/appointment/update', { oldHour, newHour });
  }

  public meetingNotify(): Observable<any> {
    return this.http.post(ApiConfig.url + '/api/appointment/meeting-notify', '');
  }

  public getMeeting(): Observable<any> {
    return this.http.get(ApiConfig.url + '/api/appointment/meeting');
  }
}
