import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../shared/auth.service';
import { MatDialogRef, MatDialog } from '@angular/material';
import { ProfileDeleteDialogComponent } from '../profile/profile-delete-dialog.component';
import { filter, tap, map } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { SetTitle } from '../actions/global.actions';
import { AppState } from '../reducers';
import { Observable, of } from 'rxjs';
import { selectError, selectLoading } from '../auth/reducders/auth.reducers';
import { Signup, SignupDone, SIGNUP_DONE, Login, LoginError, LOGIN_ERROR } from '../auth/actions/auth.actions';
import { Actions } from '@ngrx/effects';

@Component({
  selector: 'login',
  styleUrls: [
    './login.component.styl'
  ],
  templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit {
  @ViewChild('loginForm') loginForm: NgForm;

  username: string;
  needGdpr = false;
  gdpr = false;
  name: string;
  email: string;
  password: string;
  password2: string;
  message: string;
  doSignup = false;
  btnResend: boolean;
  usernameRequested = false;

  error$: Observable<string>;
  loading$: Observable<boolean>;

  constructor(
    private route: ActivatedRoute,
    private authService: AuthService,
    private dialog: MatDialog,
    private store: Store<AppState>,
    actions$: Actions
  ) {
    this.store.dispatch(new SetTitle(''));
    this.error$ = this.store.select(selectError);
    this.loading$ = this.store.select(selectLoading);

    actions$.ofType<SignupDone>(SIGNUP_DONE).subscribe(() => {
      // Successfully signed up
      this.toggleSignup();
      this.message = 'You have successfully signed up. Please verify your email address before you can login.';
      this.clear();
    });

    actions$.ofType<LoginError>(LOGIN_ERROR).pipe(
      map(res => res.payload)
    ).subscribe(res => {
      this.btnResend = res.indexOf('confirm your email address') > -1;
      this.usernameRequested = res.indexOf('Please choose a username') > -1
        || (this.usernameRequested && res.indexOf('This username is already taken.') > -1);
      this.needGdpr = res.includes('Please give us the permission to save your data.');
    });
  }

  /**
   * Toggle Signup Mode
   */
  toggleSignup(evt = null) {
    if (evt) {
      evt.preventDefault();
    }

    this.doSignup = !this.doSignup;
  }

  /**
   * Submitting the form
   */
  submit(evt = null) {
    if (evt) {
      evt.preventDefault();
    }

    if (this.doSignup) {
      this.signup();
      return;
    }
    this.login();
  }

  /**
   * A method for signing up
   */
  signup() {

    // Validations
    if (
      !this.name ||
      !this.password ||
      !this.password2 ||
      !this.email ||
      !this.username) {
      this.error$ = of('Please enter your name, both passwords and your email address.');
      return;
    }

    if (this.password !== this.password2) {
      this.error$ = of('The passwords do not match.');
      return;
    }

    this.store.dispatch(new Signup({
      name: this.name,
      password: this.password,
      email: this.email,
      username: this.username,
      acceptGdpr: this.gdpr
    }));
  }

  /**
   * Clear input fields
   */
  clear() {
    this.name = '';
    this.password = '';
    this.password2 = '';
    this.email = '';
    this.username = '';
    this.loginForm.resetForm();
    this.btnResend = false;
  }

  /**
   * Method to login the user
   */
  login() {
    this.needGdpr = false;

    // Validation
    if (!this.email || !this.password) {
      this.error$ = of('Please enter your email and password.');
      return;
    }

    // check if localStorage is available
    const storageError = this.checkLocalStorage();
    if (storageError !== null) {
      this.error$ = of(storageError);
      return;
    }

    this.store.dispatch(new Login({
      email: this.email,
      password: this.password,
      username: this.username,
      acceptGdpr: this.gdpr
    }));
  }

  deleteAccount() {
    let dialogRef: MatDialogRef<ProfileDeleteDialogComponent>;
    dialogRef = this.dialog.open(ProfileDeleteDialogComponent);

    dialogRef.afterClosed()
      .pipe(
        filter(res => !!res),
        tap(() => this.needGdpr = false),
      )
      .subscribe(() => this.store.dispatch(new Login({
        email: this.email,
        password: this.password,
        username: this.username,
        acceptGdpr: false,
        deleteAccount: true
      })));
  }

  /**
   * Resend activation email
   */
  resendEmail() {
    if (!this.email) {
      return;
    }

    this.authService.resend(this.email)
      .subscribe(
        () => {
          this.error$ = of('');
          this.btnResend = false;
          this.message = 'Email was send successfully.';
        },
        () => this.error$ = of('Failed to resend email.')
      );
  }

  /**
   * Checks if localStorage is available and accessible.
   * Source: http://stackoverflow.com/a/27081419
   * @return {string} null = ok, otherwise error message
   */
  checkLocalStorage(): string {
    if (typeof localStorage !== 'object') {
      return `Your browser does not support storing login credentials locally
        which is required for this site.`;
    }

    try {
      localStorage.setItem('localStorage', '1');
      localStorage.removeItem('localStorage');
      return null;
    } catch (e) {
      return `Your browser prevents storing login credentials locally.
      The most common cause of this is using "Private Browsing Mode".
      You need to turn that off in order to use this site.`;
    }
  }

  ngOnInit() {
    const verificationToken = this.route.snapshot.params['verify'];

    if (verificationToken) {
      this.authService
        .verify(verificationToken)
        .subscribe(
          () => this.message = 'Your email has been verified successfully. You can now login.',
          () => this.error$ = of('An error occurred. Please try clicking the verification link again or contact the IT support.')
        );
    }
  }
}
