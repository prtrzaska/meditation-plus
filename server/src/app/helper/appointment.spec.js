// eslint-disable-next-line no-unused-vars
import { app } from '../../server.conf.js';
import { expect } from 'chai';
import { AuthedSupertest } from '../helper/authed-supertest.js';
import { parseAppointmentHour, minutesUntilAppointment, findCurrentAppointmentForUser } from './appointment.js';
import timekeeper from 'timekeeper';
import moment from 'moment-timezone';
import Settings from '../models/settings.model.js';
import Appointment from '../models/appointment.model.js';
import { User } from '../models/user.model.js';
import { pick, random } from 'lodash';

let alice = new AuthedSupertest();
let mallory = new AuthedSupertest(
  'Mallory',
  'mallory',
  'mallory@sirimangalo.org'
);

describe('Appointment Helper', () => {
  describe('parseAppointmentHour()', () => {
    it('should parse valid values correctly', () => {
      expect(parseAppointmentHour(0)).to.deep.equal({ hour: 0, minute: 0 });
      expect(parseAppointmentHour(5)).to.deep.equal({ hour: 0, minute: 5 });
      expect(parseAppointmentHour(25)).to.deep.equal({ hour: 0, minute: 25 });
      expect(parseAppointmentHour(100)).to.deep.equal({ hour: 1, minute: 0 });
      expect(parseAppointmentHour(700)).to.deep.equal({ hour: 7, minute: 0 });
      expect(parseAppointmentHour(146)).to.deep.equal({ hour: 1, minute: 46 });
      expect(parseAppointmentHour(1000)).to.deep.equal({ hour: 10, minute: 0 });
      expect(parseAppointmentHour(2213)).to.deep.equal({ hour: 22, minute: 13 });
      expect(parseAppointmentHour(2359)).to.deep.equal({ hour: 23, minute: 59 });
    });
  });

  describe('minutesUntilAppointment()', () => {
    before(async () => {
      await Settings.deleteMany({});
      await Settings.create({
        appointmentsTimezone: 'America/Toronto'
      });
    });

    it('should calculate correct values for same day', async () => {
      // now weekday: Monday
      expect(await minutesUntilAppointment(
        { hour: 730, weekDay: 1 },
        moment.tz('2019-03-18T10:30:00', 'utc')
      )).to.equal(60);

      expect(await minutesUntilAppointment(
        { hour: 730, weekDay: 1 },
        moment.tz('2019-03-18T11:20:00', 'utc')
      )).to.equal(10);

      expect(await minutesUntilAppointment(
        { hour: 730, weekDay: 1 },
        moment.tz('2019-03-18T11:30:00', 'utc')
      )).to.equal(0);

      expect(await minutesUntilAppointment(
        { hour: 730, weekDay: 1 },
        moment.tz('2019-03-18T11:35:00', 'utc')
      )).to.equal(-5);

      expect(await minutesUntilAppointment(
        { hour: 730, weekDay: 1 },
        moment.tz('2019-03-18T12:35:00', 'utc')
      )).to.equal(-65);
    });

    it('should calculate correct values for overlapping day', async () => {
      expect(await minutesUntilAppointment(
        { hour: 1830, weekDay: 0 },
        moment.tz('2019-03-18T00:30:00', 'utc') // technically not overlapping
      )).to.equal(-120);

      expect(await minutesUntilAppointment(
        { hour: 1830, weekDay: 0 },
        moment.tz('2019-03-18T05:00:00', 'utc')
      )).to.equal(-(60 * 6 + 30));

      expect(await minutesUntilAppointment(
        { hour: 1830, weekDay: 5 },
        moment.tz('2019-03-21T23:15:00', 'utc')
      )).to.equal(60 * 23 + 15);

      expect(await minutesUntilAppointment(
        { hour: 1830, weekDay: 5 },
        moment.tz('2019-03-20T23:15:00', 'utc')
      )).to.equal(60 * 47 + 15);

      expect(await minutesUntilAppointment(
        { hour: 0, weekDay: 3 },
        moment.tz('2019-03-19T23:15:00', 'utc')
      )).to.equal(60 * 4 + 45);

      expect(await minutesUntilAppointment(
        { hour: 0, weekDay: 3 },
        moment.tz('2019-03-20T04:15:00', 'utc')
      )).to.equal(-15);

      expect(await minutesUntilAppointment(
        { hour: 0, weekDay: 3 },
        moment.tz('2019-03-20T03:15:00', 'utc')
      )).to.equal(45);
    });

    it('should calculate correct values for overlapping day on Saturday/Sunday', async () => {
      expect(await minutesUntilAppointment(
        { hour: 1830, weekDay: 6 },
        moment.tz('2019-03-24T05:30:00', 'utc')
      )).to.equal(-7 * 60);

      expect(await minutesUntilAppointment(
        { hour: 1830, weekDay: 0 },
        moment.tz('2019-03-23T23:00:00', 'utc')
      )).to.equal(60 * 23 + 30);

      expect(await minutesUntilAppointment(
        { hour: 1830, weekDay: 0 },
        moment.tz('2019-03-22T22:30:00', 'utc')
      )).to.equal(-(60 * 24 * 5));
    });
  });

  describe('findCurrentAppointmentForUser()', () => {
    let testAppointments = [
      { user: null, weekDay: 1, hour: 700 },
      { user: null, weekDay: 2, hour: 700 },
      { user: null, weekDay: 3, hour: 700 },
      { user: null, weekDay: 4, hour: 700 },
      { user: null, weekDay: 5, hour: 700 },
      { user: null, weekDay: 6, hour: 700 },
      { user: null, weekDay: 0, hour: 700 },
      { user: null, weekDay: 0, hour: 1830 },
      { user: null, weekDay: 3, hour: 1830 },
      { user: null, weekDay: 4, hour: 1830 },
      { user: null, weekDay: 5, hour: 1830 },
      { user: null, weekDay: 6, hour: 1830 }
    ];

    /**
     * Method for removing the user property from all appointments
     * and setting the user of one specific appointment.
     */
    const bookAppointment = async (user, weekDay, hour) => {
      await Appointment.updateMany({}, { user: null });
      return await Appointment.findOneAndUpdate({ weekDay, hour }, { user }, {
        new: true,
        projection: ['hour', 'weekDay', 'user']
      }).lean();
    };

    alice.authorize();
    mallory.authorize();

    before(async () => {
      await Appointment.deleteMany({});
      await Settings.deleteMany({});

      await Settings.create({
        appointmentsTimezone: 'America/Toronto'
      });

      await Appointment.insertMany(testAppointments);
    });

    after(async () => {
      await User.deleteMany({});
      await Appointment.deleteMany({});
      await Settings.deleteMany({});
    });

    it('should return null for invalid user', async () => {
      expect(await findCurrentAppointmentForUser(null, 0, 0)).to.equal(null);
      expect(await findCurrentAppointmentForUser({ a: 2 }, 0, 0)).to.equal(null);

      await bookAppointment(alice.user._id, 0, 1830);

      timekeeper.travel(moment.tz('2019-03-24 18:30:00', 'America/Toronto').toDate());
      expect(await findCurrentAppointmentForUser(mallory.user, 5, 5)).to.equal(null);

      timekeeper.travel(moment.tz('2018-10-14 10:58:00', 'America/Toronto').toDate());
      expect(await findCurrentAppointmentForUser(mallory.user, 5, 5)).to.equal(null);
    });

    it('should find appointment for valid user in same timezone', async () => {
      /**
       * First test in Winter (EST)
       */
      timekeeper.travel(moment.tz('2019-12-28T18:30:00', 'America/Toronto').format());
      expect(await findCurrentAppointmentForUser(alice.user, 5, 5)).to.equal(null);

      const appEST = await bookAppointment(alice.user._id, 6, 1830);

      expect(pick(await findCurrentAppointmentForUser(alice.user, 5, 5), ['_id', 'hour', 'weekDay', 'user']))
        .to.deep.equal(appEST);

      timekeeper.travel(moment.tz('2019-12-28T18:34:00', 'America/Toronto').format());
      expect(pick(await findCurrentAppointmentForUser(alice.user, 5, 5), ['_id', 'hour', 'weekDay', 'user']))
        .to.deep.equal(appEST);

      timekeeper.travel(moment.tz('2019-12-28T18:25:00', 'America/Toronto').format());
      expect(pick(await findCurrentAppointmentForUser(alice.user, 5, 5), ['_id', 'hour', 'weekDay', 'user']))
        .to.deep.equal(appEST);

      timekeeper.travel(moment.tz('2019-12-29T00:29:00', 'America/Toronto').format());
      expect(pick(await findCurrentAppointmentForUser(alice.user, 5, 60 * 6), ['_id', 'hour', 'weekDay', 'user']))
        .to.deep.equal(appEST);

      timekeeper.travel(moment.tz('2019-12-28T18:22:00', 'America/Toronto').format());
      expect(await findCurrentAppointmentForUser(alice.user, 5, 5)).to.equal(null);

      timekeeper.travel(moment.tz('2019-12-28T18:07:00', 'America/Toronto').format());
      expect(await findCurrentAppointmentForUser(alice.user, 5, 5)).to.equal(null);

      /**
       * Second test in Summer (EDT)
       */
      timekeeper.travel(moment.tz('2019-07-23T07:00:00', 'America/Toronto').format());
      expect(await findCurrentAppointmentForUser(alice.user, 5, 5)).to.equal(null);

      const appEDT = await bookAppointment(alice.user._id, 2, 700);

      expect(pick(await findCurrentAppointmentForUser(alice.user, 5, 5), ['_id', 'hour', 'weekDay', 'user']))
        .to.deep.equal(appEDT);

      timekeeper.travel(moment.tz('2019-07-23T07:05:00', 'America/Toronto').format());
      expect(pick(await findCurrentAppointmentForUser(alice.user, 5, 5), ['_id', 'hour', 'weekDay', 'user']))
        .to.deep.equal(appEDT);

      timekeeper.travel(moment.tz('2019-07-23T06:55:00', 'America/Toronto').format());
      expect(pick(await findCurrentAppointmentForUser(alice.user, 5, 5), ['_id', 'hour', 'weekDay', 'user']))
        .to.deep.equal(appEDT);

      timekeeper.travel(moment.tz('2019-07-23T06:54:00', 'America/Toronto').format());
      expect(await findCurrentAppointmentForUser(alice.user, 5, 5)).to.equal(null);

      timekeeper.travel(moment.tz('2019-07-23T07:07:00', 'America/Toronto').format());
      expect(await findCurrentAppointmentForUser(alice.user, 5, 5)).to.equal(null);
    });

    it('should find appointment for user from different timezone', async () => {
      /**
       * France
       */
      timekeeper.travel(moment.tz('2019-06-30T13:00:00', 'Europe/Paris').format());

      expect(await findCurrentAppointmentForUser(alice.user, 5, 5)).to.equal(null);

      const appParis = await bookAppointment(alice.user._id, 0, 700);

      expect(pick(await findCurrentAppointmentForUser(alice.user, 5, 5), ['_id', 'hour', 'weekDay', 'user']))
        .to.deep.equal(appParis);

      timekeeper.travel(moment.tz('2019-06-30T13:03:00', 'Europe/Paris').format());
      expect(pick(await findCurrentAppointmentForUser(alice.user, 5, 5), ['_id', 'hour', 'weekDay', 'user']))
        .to.deep.equal(appParis);

      timekeeper.travel(moment.tz('2019-06-30T12:56:00', 'Europe/Paris').format());
      expect(pick(await findCurrentAppointmentForUser(alice.user, 5, 5), ['_id', 'hour', 'weekDay', 'user']))
        .to.deep.equal(appParis);

      timekeeper.travel(moment.tz('2019-06-30T13:08:00', 'Europe/Paris').format());
      expect(await findCurrentAppointmentForUser(alice.user, 5, 5)).to.equal(null);

      timekeeper.travel(moment.tz('2019-06-30T12:53:00', 'Europe/Paris').format());
      expect(await findCurrentAppointmentForUser(alice.user, 5, 5)).to.equal(null);


      /**
       * Thailand
       */
      timekeeper.travel(moment.tz('2019-12-30T06:30:00', 'Asia/Bangkok').format());

      expect(await findCurrentAppointmentForUser(alice.user, 5, 5)).to.equal(null);

      const appBangkok = await bookAppointment(alice.user._id, 0, 1830);

      expect(pick(await findCurrentAppointmentForUser(alice.user, 5, 5), ['_id', 'hour', 'weekDay', 'user']))
        .to.deep.equal(appBangkok);

      timekeeper.travel(moment.tz('2019-12-30T06:26:00', 'Asia/Bangkok').format());
      expect(pick(await findCurrentAppointmentForUser(alice.user, 5, 5), ['_id', 'hour', 'weekDay', 'user']))
        .to.deep.equal(appBangkok);

      timekeeper.travel(moment.tz('2019-12-30T06:38:00', 'Asia/Bangkok').format());
      expect(await findCurrentAppointmentForUser(alice.user, 5, 5)).to.equal(null);

      timekeeper.travel(moment.tz('2019-12-30T06:23:00', 'Asia/Bangkok').format());
      expect(await findCurrentAppointmentForUser(alice.user, 5, 5)).to.equal(null);
    });

    it('should pass generic tests for random timezones', async () => {
      const getRandomTimezone = () => moment.tz.names()[random(0, moment.tz.names().length - 1)];

      const NUM_ITERATIONS = 5;

      // test for each timezone that could be set in Settings
      for (let i = 0; i <= NUM_ITERATIONS; i++) {
        const zone = getRandomTimezone();

        await Settings.findOneAndUpdate({}, {
          appointmentsTimezone: zone
        });

        for (let appoint of testAppointments.slice(0, 1)) {
          await bookAppointment(alice.user._id, appoint.weekDay, appoint.hour);
          appoint = { ...appoint, user: alice.user._id };

          const { minute, hour } = parseAppointmentHour(appoint.hour);
          const appointmentTime = moment.tz(zone).set({
            weekday: appoint.weekDay,
            hour,
            minute,
            second: 0,
            millisecond: 0
          });

          // test each timezone that a user could have
          for (let j = 0; j <= NUM_ITERATIONS; j++) {
            const zoneUser = getRandomTimezone();
            const userTime = moment.tz(appointmentTime, zoneUser);

            // add minutes and test function
            for (const x of [-1500, -1000, -500, -200, -50, -10, -5, -3, -1, 0]) {
              timekeeper.travel(userTime.clone().add(x, 'minutes').format());
              expect(
                pick(await findCurrentAppointmentForUser(alice.user, 5, 7), ['user', 'weekDay', 'hour'])
              ).to.deep.equal(
                !!appoint.user && -5 <= x ? appoint : {}
              );

              timekeeper.travel(userTime.clone().add(-x, 'minutes').format());
              expect(
                pick(await findCurrentAppointmentForUser(alice.user, 5, 7), ['user', 'weekDay', 'hour'])
              ).to.deep.equal(
                !!appoint.user && 7 >= -x ? appoint : {}
              );
            }
          }
        }
      }
    }).timeout(10000);

  });
});
