import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../user';
import { Store } from '@ngrx/store';
import { AppState } from '../../reducers';
import { MatSnackBar } from '@angular/material';
import { ThrowError } from '../../actions/global.actions';

@Component({
  selector: 'user-admin-form',
  templateUrl: './user-admin-form.component.html'
})
export class UserAdminFormComponent {

  user: any;
  loading = false;

  constructor(
    private userService: UserService,
    private router: Router,
    private store: Store<AppState>,
    private snackbar: MatSnackBar,
    route: ActivatedRoute
  ) {
    if (route.snapshot.params['id']) {
      this.userService
        .get(route.snapshot.params['id'])
        .subscribe(res => this.user = res);
    }
  }

  submit() {
    this.loading = true;
    console.log(this.user);
    this.userService
      .save(this.user)
      .subscribe(
        () => {
          this.snackbar.open(
            'The user has been saved successfully.',
            'VIEW PROFILE',
            { duration: 5000 }
          ).onAction()
          .subscribe(() => this.router.navigate(['/profile', this.user.username]));
          this.router.navigate(['/admin/users']);
        },
        error => {
          this.loading = false;
          this.store.dispatch(new ThrowError({ error }));
        },
        () => this.loading = false
      );
  }
}
